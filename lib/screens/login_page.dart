import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:system2/screens/signup_page.dart';
import 'package:system2/utils/repo.dart';
import 'package:system2/utils/validators.dart';
import 'package:system2/widgets/custom_button.dart';
import 'package:system2/widgets/text_field_widget.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email;

  String _pass;

  GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  bool showNow = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(milliseconds: 50),(){
      setState(() {
        showNow = true;
      });
    });
  }
  Widget _simpleAnimation({Widget child}){
    return AnimatedPadding(
      duration: Duration(milliseconds: 500),
      padding: EdgeInsets.only(top: showNow ? 0.0 : 20.0),
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 1000),
        opacity: showNow ? 1.0 : 0.0,
        child: child,
      ),
    );
  }
  Widget _animatedOpacity(Widget child){
    return AnimatedOpacity(
      duration: Duration(milliseconds: 2000),
      opacity: showNow ? 1.0 : 0.1,
      child: child,
    );
  }
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    Orientation orien = MediaQuery.of(context).orientation;
    bool screen = orien == Orientation.portrait ? true : false;
    return Scaffold(
      body: Container(
        height: h,
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Form(
              key: _keyForm,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 100,),
                  _animatedOpacity(Text(
                    "Welcome to Login",style: TextStyle(
                      shadows: [
                        Shadow(
                          offset: Offset(1.0, 5.0),
                          blurRadius: 1.0,
                          color: Utils.baseColr.withOpacity(.2),
                        ),
                      ],
                      color: Utils.baseColr,fontWeight: FontWeight.bold,fontSize:30),)),
                  SizedBox(height: 40,),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      labelText: 'Email',
                      hintText: 'example@mail.com',
                      validator: Validators().validateEmail,
                      onSaved: (v){
                        _email = v;
                      },
                    ),
                  ),
                  SizedBox(height: 40,),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      labelText: 'Password',
                      hintText: '********',
                      obsecure: true,
                      validator: Validators().validatePassword,
                      onSaved: (v){
                        _pass = v;
                      },
                    ),
                  ),
                  SizedBox(height: 40,),
                  _simpleAnimation(
                    child: CustomButton(
                      w1: w*.9,
                      h2: 50,
                      text:   'Login',
                      onPressed:  (){
//                Navigator.push(context, Utils.createRoute(page: IntrestPage()));
                        FocusScope.of(context).requestFocus(FocusNode());
                        final FormState form = _keyForm.currentState;
                        if(form.validate()){
                          form.save();

                        }
                      },
                    ),
                  ),
                  SizedBox(height: 30,),
                  _simpleAnimation(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Utils.baseColr,
                          ),
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text("OR",style: TextStyle(color: Utils.baseColr),),
                        SizedBox(
                          width: 20,
                        ),
                        Expanded(
                          child: Container(
                            height: 1,
                            color: Utils.baseColr,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 30,),
                  _simpleAnimation(
                    child: CustomButton(
                      w1: w*.9,
                      h2: 50,
                      text:  'Register',
                      onPressed:  (){
                        Navigator.push(context, PageTransition(child: SignUpPage()));
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
