import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:system2/utils/repo.dart';
import 'package:system2/utils/validators.dart';
import 'package:system2/widgets/custom_button.dart';
import 'package:system2/widgets/text_field_widget.dart';


class SignUpPage extends StatefulWidget {
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  String _name;
  String _userName;
  String _email;
  String _pass;
  GlobalKey<FormState> _keyForm = GlobalKey<FormState>();
  bool showNow = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration(milliseconds: 50),(){
      setState(() {
        showNow = true;
      });
    });
  }
  Widget _simpleAnimation({Widget child}){
    return AnimatedPadding(
      duration: Duration(milliseconds: 500),
      padding: EdgeInsets.only(top: showNow ? 0.0 : 20.0),
      child: AnimatedOpacity(
        duration: Duration(milliseconds: 1000),
        opacity: showNow ? 1.0 : 0.0,
        child: child,
      ),
    );
  }
  Widget _animatedOpacity(Widget child){
    return AnimatedOpacity(
      duration: Duration(milliseconds: 2000),
      opacity: showNow ? 1.0 : 0.1,
      child: child,
    );
  }
  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;
    double l = MediaQuery.of(context).size.longestSide;
    Orientation orien = MediaQuery.of(context).orientation;
    bool screen = orien == Orientation.portrait ? true : false;
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Form(
              key: _keyForm,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 100),
                  Text(
                    "Welcome to SignUp",style: TextStyle(
                      shadows: [
                        Shadow(
                          offset: Offset(1.0, 5.0),
                          blurRadius: 1.0,
                          color: Utils.baseColr.withOpacity(.2),
                        ),
                      ],
                      color: Utils.baseColr,fontWeight: FontWeight.bold,fontSize:30),),
                  SizedBox(height: screen?h*.03:w*.03,),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      labelText: 'Name',
                      hintText: 'email here',
                      validator: Validators().validateName,
                      onSaved: (v){
                        _name = v;
                      },
                    ),
                  ),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      labelText: 'Email',
                      hintText: 'example@mail.com',
                      validator: Validators().validateEmail,
                      onSaved: (v){
                        _email = v;
                      },
                    ),
                  ),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      labelText: 'Number',
                      hintText: 'example@mail.com',
                      validator: Validators().validateEmail,
                      onSaved: (v){
                        _email = v;
                      },
                    ),
                  ),
                  SizedBox(height: screen?h*.03:w*.03,),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      obsecure: true,
                      labelText: 'Password',
                      hintText: '********',
                      validator: Validators().validatePassword,
                      onSaved: (v){
                        _pass = v;
                      },
                    ),
                  ),
                  SizedBox(height: screen?h*.03:w*.03,),
                  _simpleAnimation(
                    child: CustomTextFieldWidget(
                      obsecure: true,
                      labelText: 'Confirm Password',
                      hintText: '********',
                      validator: Validators().validatePassword,
                      onSaved: (v){
                        _pass = v;
                      },
                    ),
                  ),
                  SizedBox(height: screen?h*.06:w*.06,),
                  _simpleAnimation(
                    child: CustomButton(
                      w1: w,
                      text:  'Register',
                      onPressed:  (){
                        FocusScope.of(context).requestFocus(FocusNode());
                        final FormState form = _keyForm.currentState;
                        if(form.validate()){
                          form.save();
//                        blocSign.signUp(context, _name, _userName, _pass, bloc.image, _email, "0", widget.tabs);
                        }
                      },
                    ),
                  ),
                  SizedBox(height: 20,),
                  _animatedOpacity(
                    Text.rich(
                      TextSpan(
                          text: "Already have an account? ",
                          children: [
                            TextSpan(
                              text: "LOGIN",
                              style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold),
                              recognizer: TapGestureRecognizer()..onTap = () => Navigator.pop(context),
                            ),
                          ],
                          style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold,color: Utils.baseColr)
                      ),
                    ),
                  ),
                  SizedBox(height: 50,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
